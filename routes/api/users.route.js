const express = require("express");
const router = express.Router();

const { loginUser, registerUser } = require("../controllers/users.controller");

router.get("/test", (req, res) => res.json({ msg: "User Works!!" }));
router.post("/login", loginUser);
router.post("/register", registerUser);

module.exports = router;
