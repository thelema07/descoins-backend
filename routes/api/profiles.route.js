const express = require("express");
const router = express.Router();
const passport = require("passport");

const {
  test,
  addProfile,
  getCurrentProfile
} = require("../controllers/profile.controller");

router.get("/test", test);
router.post("/", passport.authenticate("jwt", { session: false }), addProfile);
router.get(
  "/",
  passport.authenticate("jwt", { session: false }),
  getCurrentProfile
);

module.exports = router;
