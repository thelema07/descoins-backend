const express = require("express");
const router = express.Router();
const passport = require("passport");

const {
  getCurrencyConversion
} = require("../controllers/currency.controllers");

router.get("/", (req, res) => res.json({ msg: "OK!!" }));
router.get(
  "/convert/:currency1/:currency2/:value1",
  passport.authenticate("jwt", { session: false }),
  getCurrencyConversion
);

module.exports = router;
