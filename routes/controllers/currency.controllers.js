const ForgeClient = require("forex-quotes").default;

const Profile = require("../../models/Profile");

let client = new ForgeClient("ER4vUGjQSkj05T6HhwAzmCVTUDHORODX");
client.connect();

exports.getCurrencyConversion = async (req, res) => {
  let from = req.params.currency1;
  let to = req.params.currency2;
  let funds = req.params.value1;
  await client
    .convert(from, to, funds)
    .then(response => {
      if (typeof response.value == "number") {
        Profile.findOne({ user: req.user.id }).then(profile => {
          const newConversion = {
            from: from,
            to: to,
            origin: funds,
            result: response.value,
            date: Date.now
          };

          profile.conversion.unshift(newConversion);

          profile.save().then(profile => res.json(response));
        });
      }
    })
    .catch(err => {
      res.json(err);
    });
};
