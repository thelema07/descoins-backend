const Profile = require("../../models/Profile");

const validateProfileInput = require("../../validation/validateProfile");

exports.test = (req, res) => {
  res.json({ msg: "this is profile routes" });
};

exports.addProfile = (req, res) => {
  const { errors, isValid } = validateProfileInput(req.body);

  if (!isValid) {
    return res.status(400).json(errors);
  }

  const profileFields = {};
  profileFields.user = req.user.id;
  if (req.body.handle) profileFields.handle = req.body.handle;
  if (req.body.name) profileFields.name = req.body.name;
  if (req.body.country) profileFields.country = req.body.country;
  if (req.body.location) profileFields.location = req.body.location;
  if (req.body.description) profileFields.description = req.body.description;
  if (req.body.gender) profileFields.gender = req.body.gender;
  if (req.body.twitter) profileFields.twitter = req.body.twitter;
  if (req.body.facebook) profileFields.facebook = req.body.facebook;
  if (req.body.youtube) profileFields.youtube = req.body.youtube;

  Profile.findOne({ user: req.user.id }).then(profile => {
    if (profile) {
      Profile.findOneAndUpdate(
        { user: req.user.id },
        { $set: profileFields },
        { new: true }
      ).then(profile => res.json(profile));
    } else {
      Profile.findOne({ handle: profileFields.handle }).then(profile => {
        if (profile) {
          errors.handle = "That handle already exists";
          res.status(400).json(errors);
        }

        new Profile(profileFields).save().then(profile => res.json(profile));
      });
    }
  });
};

exports.getCurrentProfile = (req, res) => {
  const errors = {};

  Profile.findOne({ user: req.user.id })
    .populate("user", ["name"])
    .then(profile => {
      if (!profile) {
        errors.noprofile = "This user does not have a profile";
        return res.status(404).json(errors);
      }
      res.json(profile);
    })
    .catch(err => res.status(404).json(err));
};
