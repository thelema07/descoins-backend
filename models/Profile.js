const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const ProfileSchema = new Schema({
  user: { type: Schema.Types.ObjectId, ref: "users" },
  handle: { type: String, required: true, max: 40 },
  name: { type: String, required: true },
  country: { type: String, required: true },
  location: { type: String },
  gender: { type: String },
  description: { type: String },
  twitter: { type: String },
  facebook: { type: String },
  youtube: { type: String },
  conversion: { type: Array },
  date: { type: Date, default: Date.now }
});

module.exports = Profile = mongoose.model("profile", ProfileSchema);
